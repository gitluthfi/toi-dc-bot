require("dotenv").config();
const config = require("./config")

const Discord = require ("djst-client");
const client = new Discord.Client({
    intents: ["GUILDS", "GUILD_MESSAGES"],
    prefix: config.prefix,
    initCommands: true
})

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`)
})
client.on('messageCreate', async (msg) => {
    if (msg.content === 'ping') {
      await msg.reply('Pong!');
    }
  });

//client.on('messageCreate', message => {
  // Check if the message content is 'ping'
  //if (message.content === 'ping') {
    // Send a response message with 'pong'
    //message.channel.send('pong');
  //}
//});

client.on('message', async message => {
  if (message.content === '!startthread') {
    const channel = message.channel;
    const thread = await channel.threads.create({
      name: 'New Thread',
      autoArchiveDuration: 60,
      reason: 'Thread started by user'
    });
    message.reply(`New thread created! Click on ${thread} to view.`);
  }
});

client.login(process.env.TOKEN);
